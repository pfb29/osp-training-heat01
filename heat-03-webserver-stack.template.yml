---

heat_template_version: 2016-10-14

description: >
  HOT template to boot a bastion+webserver stack, plus all network infrastructure

parameters:
  bastion_instance_name:
    default: "bastion-heat"
    type: string
    description: Name to apply to the bastion instance
  webserver_instance_name:
    default: "webserver-heat"
    type: string
    description: Name to apply to the webserver instance 
  instance_image:
    default: CentOS-7-x86_64-GenericCloud
    type: string
    description: Name of image to use
    constraints:
      - allowed_values:
          - CentOS-7-x86_64-GenericCloud
  instance_flavor:
    default: c1.training
    type: string
    description: Flavor to use for instance
    constraints:
      - allowed_values:
          - c1.training
  ssh_public_key:
    type: string
    description: Public key to inject into instance
    default: "testing-master"
  external_network:
    default: CUDN-Private
    type: string
    description: External Network
    constraints:
      - allowed_values:
          - CUDN-Private

resources:
  external_bastion_heat_sg:
    type: OS::Neutron::SecurityGroup
    properties:
      name: external_bastion_heat_sg
      description: External firewall rules for the controller node
      rules:
        - protocol: tcp
          remote_ip_prefix: 0.0.0.0/0
          port_range_min: 22
          port_range_max: 22
        - protocol: icmp
          remote_ip_prefix: 0.0.0.0/0

  internal_bastion_heat_sg:
    type: OS::Neutron::SecurityGroup
    properties:
      name: internal_bastion_heat__sig
      description: SG to tag on internal instances for Bastion traffic
      rules:
        - protocol: icmp
          remote_ip_prefix: 0.0.0.0/0

  external_webserver_heat_sg:
    type: OS::Neutron::SecurityGroup
    properties:
      name: external_webserver_heat_sg
      description: Allow external traffic to webservers
      rules:
        - protocol: tcp
          port_range_min: 80
          port_range_max: 80
          remote_ip_prefix: 0.0.0.0/0
        - protocol: tcp
          port_range_min: 443
          port_range_max: 443
          remote_ip_prefix: 0.0.0.0/0
        - protocol: icmp
          remote_ip_prefix: 0.0.0.0/0

  internal_webserver_heat_sg:
    type: OS::Neutron::SecurityGroup
    properties:
      name: internal_webserver_heat_sg
      description: Allow internal bastion SSH traffic to webservers
      rules:
        - protocol: tcp
          port_range_min: 22
          port_range_max: 22
          remote_mode: remote_group_id
          remote_group_id: { get_resource: internal_bastion_heat_sg }
        - protocol: icmp
          remote_ip_prefix: 0.0.0.0/0

  private-network:
    type: OS::Neutron::Net
    properties:
      name: "private-network"

  private-subnet:
    type: OS::Neutron::Subnet
    properties:
      name: "private-subnet"
      network_id: { get_resource: private-network }
      cidr: "192.168.1.0/24"
      dns_nameservers: ["131.111.8.42","131.111.12.20"]
      allocation_pools:
        - start: 192.168.1.10
          end: 192.168.1.20

  router:
    type: OS::Neutron::Router
    properties:
      name: "CUDN-Private Heat Router"
      external_gateway_info:
        network: { get_param: external_network }

  router_interface:
    type: OS::Neutron::RouterInterface
    properties:
      router_id: { get_resource: router }
      subnet_id: { get_resource: private-subnet }

  bastion_instance:
    type: OS::Nova::Server
    properties:
      name: { get_param: bastion_instance_name }
      flavor: { get_param: instance_flavor }
      image: { get_param: instance_image }
      key_name: { get_param: ssh_public_key }
      networks:
        - network: {get_resource: private-network }
      security_groups:
        - { get_resource: external_bastion_heat_sg }
        - { get_resource: internal_bastion_heat_sg }

  webserver_instance:
    type: OS::Nova::Server
    properties:
      name: { get_param: webserver_instance_name }
      flavor: { get_param: instance_flavor }
      image: { get_param: instance_image }
      key_name: { get_param: ssh_public_key }
      networks:
        - network: {get_resource: private-network }
      security_groups:
        - { get_resource: external_webserver_heat_sg }
        - { get_resource: internal_webserver_heat_sg }
      user_data_format: RAW
      user_data:
         get_resource: webserver_init

  webserver_init:
    type: OS::Heat::CloudConfig
    properties:
      cloud_config:
        package_update: true
        runcmd:
          - sudo systemctl enable httpd
          - sudo systemctl start httpd
          - [ wget, "https://bit.ly/2DMdN7O", -O, "/home/centos/content.tar.gz" ]
          - sleep 5
          - [ chown, "centos:centos", "/home/centos/content.tar.gz" ]
          - [ sudo, tar, zxvf, "/home/centos/content.tar.gz", -C, "/var/www/html/" ]
        packages:
          - epel-release
          - wget
          - vim
          - git
          - httpd

  bastion_floating_ip:
    type: OS::Neutron::FloatingIP
    properties:
      floating_network: { get_param: external_network }

  bastion_floating_ip_association:
    type: OS::Nova::FloatingIPAssociation
    properties:
      floating_ip: { get_resource: bastion_floating_ip }
      server_id: { get_resource: bastion_instance }

  webserver_floating_ip:
    type: OS::Neutron::FloatingIP
    properties:
      floating_network: { get_param: external_network }

  webserver_floating_ip_association:
    type: OS::Nova::FloatingIPAssociation
    properties:
      floating_ip: { get_resource: webserver_floating_ip }
      server_id: { get_resource: webserver_instance }

outputs:
  bastion_instance_public_ip:
    description: Public floating IP address of the bastion instance on CUDN-Private
    value: { get_attr: [ bastion_floating_ip, floating_ip_address ] }
  webserver_instance_private_ip:
    description: Private IP address of the webserver instance on the private network
    value: { get_attr: [ webserver_instance, first_address ] }
  webserver_instance_public_ip:
    description: Public floating IP address of the webserver instance on CUDN-Private
    value: { get_attr: [ webserver_floating_ip, floating_ip_address ] }
