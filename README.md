### OSP Training Materials: Heat Orchestration #01

This repo contains some Heat Orchestration Templates for use in teaching the course material 
of the Research Computing: Infrastructure as a Service course.

## heat-01-cirros.template.yml

Simple HOT to create a single instance on pre-existing networks

## heat-02-bastion.template.yml

HOT to create a bastion login instance and all needed network infrastructure

##heat-03-webserver-stack.template.yml

HOT to create a bastion+webserver stack, networking infrastructure, and configure the webserver
